# Estructuras de repetición - Juego de palitos

![main1.png](images/main1.png)
![main2.png](images/main2.png)
![main3.png](images/main3.png)



Una de las ventajas de utilizar programas de computadoras es que podemos realizar tareas repetitivas fácilmente. Los ciclos como `for`, `while`, y `do-while` son  estructuras de control que nos permiten repetir un conjunto de instrucciones. A estas estructuras también se les llama *estructuras de repetición*.  En la experiencia de laboratorio de hoy, practicarás el uso de ciclos para implementar el juego de palitos en C++.


## Objetivos:

1. Diseñar algoritmos para realizar una tarea.
2. Traducir un algoritmo a pseudocódigo.
3. Implementar un programa completo.
4. Practicar el uso de estructuras de repetición.


Esta experiencia de laboratorio es una adaptación del ejercicio en: http://nifty.stanford.edu/2014/laaksonen-vihavainen-game-of-sticks/handout.html.

## Pre-Lab:

Antes de llegar al laboratorio debes haber:

1. Repasado los conceptos básicos relacionados a estructuras de repetición.

2. Estudiado los conceptos e instrucciones para la sesión de laboratorio.

3. Tomado el quiz Pre-Lab, disponible en Moodle.

---

---

## Palitos (Game of Sticks)

El juego de los palitos se juega entre dos contrincantes. Al principio del juego hay un montículo de palitos en la mesa. En cada turno, cada jugador retira de 1 a 3 palitos de la mesa. El jugador que recoge el último palito pierde el juego.

Por ejemplo, un juego de palitos podría transcurrir así:

* El juego comienza con 20 palitos en la mesa.
* Aurora retira 3 palitos, faltan 17.
* Bruno retira 2 palitos, faltan 15.
* Aurora retira 1 palito, faltan 14.
* Bruno retira 3 palitos, faltan 11.
* Aurora retira 2 palitos, faltan 9.
* Bruno retira 2 palitos, faltan 7.
* Aurora retira 3 palitos, faltan 4.
* Bruno retira 1 palito, faltan 3.
* Aurora retira 2 palitos, falta 1.
* Bruno tiene que retirar el único palito restante y pierde el juego.


---

---


## Sesión de laboratorio:

En la experiencia de laboratorio de hoy practicarás el uso de ciclos para implementar el juego de palitos.


### Ejercicio 1 - Diseñar el algoritmo

Diseña un algoritmo para que dos personas puedan jugar el juego de los palitos en la computadora. El algoritmo debe respetar las siguientes restricciones:

1. Al principio debe pedir el número de palitos que habrá en la mesa. El rango de números aceptados será de [10,100]. Si el usuario entra un número fuera de ese rango, el programa continuará preguntando.

2. En cada jugada, se permitirá que el jugador retire de 1 a 3 palitos (sin exceder el número de palitos disponibles en la mesa). Si el usuario entra un número fuera de tal rango, el programa continuará preguntando.

3. Al finalizar el juego, el programa debe informar quién fue el ganador.

**Ejemplo:**

```
¡Bienvenidos al juego de palitos!

Entre el numero inicial de palitos [10,100]: 150
Entre el numero inicial de palitos [10,100]: 10

Hay 10 palitos en la mesa.
Jugador 1 - Entre el numero de palitos a retirar (1-3): 4
Jugador 1 - Entre el numero de palitos a retirar (1-3): 3

Hay 7 palitos en la mesa.
Jugador 2 - Entre el numero de palitos a retirar (1-3): 3

Hay 4 palitos en la mesa.
Jugador 1 - Entre el numero de palitos a retirar (1-3): 2

Hay 2 palitos en la mesa.
Jugador 2 - Entre el numero de palitos a retirar (1-2): 3
Jugador 2 - Entre el numero de palitos a retirar (1-2): 1

Hay 1 palito en la mesa.
Jugador 1 - Entre el numero de palitos a retirar (1-1): 1
Perdiste Jugador 1 :-(
```


### Ejercicio 2 - Escribir el pseudocódigo

#### Instrucciones

1. Cierra la computadora.

2. Expresa el algoritmo en pseudocódigo. 

3. Ejecuta a mano el pseudocódigo para capturar errores lógicos. Trátalo con varios casos *extremos* y escribe sus resultados a mano. [Aquí](https://docs.google.com/a/upr.edu/presentation/d/1m-lXCUTKhCZC8vTnmtAXwfxQjIKkSVhTUrcywiZsdbQ/edit?usp=sharing) puedes ver un ejemplo de como hacer "hand-checking" a un algoritmo.


### Ejercicio 3 - Implementar el código

#### Instrucciones

1. Abre la computadora.

2. Implementa el algoritmo en un programa en C++.

**Ayudas:**

1. Utiliza una variable de tipo `int` para llevar cuenta del jugador. Al final de cada turno, intercambia el jugador así:

    ```cpp
    jugador = jugador == 1 ? 2 : 1;
    ```

    o enumera los jugadores como 0 y 1 y usa tu maestría en operador módulo:

    ```cpp
    jugador = (jugador + 1) % 2;
    ```

2. Piensa en los 3 mandamientos de las estructuras de repetición:

    a. ¿Cuál es la variable de control del ciclo y qué condición será la que mantiene el ciclo iterando? 

    b. ¿A qué se inicializa la variable de control del ciclo?
    
    c. ¿Cómo se modifica la variable de control del ciclo en cada iteración?


### Entregas

1. Utiliza "Entrega 1" en Moodle para entregar el pseudocódigo del juego de palitos.

2. Utiliza "Entrega 2" en Moodle para entregar el papel donde tú y tu pareja hicieron la ejecución a mano del pseudocódigo del juego de palitos.

3. Utiliza "Entrega 3" en Moodle para entregar un archivo `.cpp` con el programa en C++ para jugar palitos. El programa no debe tener errores de compilación, debe estar debidamente documentado y funcionar como se supone. Recuerda utilizar buenas prácticas de programación, incluye el nombre de los programadores y documenta tu programa.



---

---

## Referencias

[1] http://nifty.stanford.edu/2014/laaksonen-vihavainen-game-of-sticks/handout.html

[2] http://www.gardengames.com/acatalog/509_Garden_Games_Giant_Pick-Up_Sticks.html

[3] http://www.toves.org/books/java/ch05-while/

[4] http://forum.codecall.net/topic/35425-pseudocode-tutorial-the-basics/page-2

---

---

---
