# Repetition Structures - Game of Sticks

![main1.png](images/main1.png)
![main2.png](images/main2.png)
![main3.png](images/main3.png)



One of the advantages of using computer programs is that we can easily implement repetitive tasks. Structures such as the `for`, `while`, and `do-while` allow us to repeat a block of instructions as many times as needed. These structures are also known as *repetition structures*. In today’s laboratory experience, you will practice the use of loops to implement a game of sticks in C++.

## Objectives:

1. Design algorithms to realize a task.
2. Translate an algorithm into pseudocode.
3. Implement a complete program.
4. Practice the use of repetition structures.

This laboratory experience is an adaptation of the exercise found in: http://nifty.stanford.edu/2014/laaksonen-vihavainen-game-of-sticks/handout.html.

## Pre-Lab:

Before coming to the laboratory session you should have:

1. Reviewed the basic concepts related to repetition structures.

2. Studied the concepts and instructions for the laboratory session.

3. Taken the Pre-Lab quiz available through the course’s Moodle portal.

---

---

## Game of Sticks

The game of sticks is played between two contestants. At the beginning of the game there is a pile of sticks on the table. In each turn, each player takes from 1 to 3 sticks from the table. The player that picks up the last stick loses the game.

For example, a game of sticks could go like this:

* The game starts with 20 sticks on the table.
* Aurora takes 3 sticks, there are 17 left.
* Bruno takes 2 sticks, there are 15 left.
* Aurora takes 1 stick, there are 14 left.
* Bruno takes 3 sticks, there are 11 left.
* Aurora takes 2 sticks, there are 9 left.
* Bruno takes 2 sticks, there are 7 left.
* Aurora takes 3 sticks, there are 4 left.
* Bruno takes 1 stick, there are 3 left.
* Aurora takes 2 sticks, there is 1 left.
* Bruno has to take the last stick and loses the game.

---

---


## Laboratory Session:

In today’s laboratory experience you will practice the use of loops to implement a game of sticks.

### Exercise 1 - Design the Algorithm

Design an algorithm so that two people can play the game of sticks on the computer. The algorithm should have these restrictions:

1. At the beginning, it should ask the number of sticks that will be on the table. The range of accepted numbers is [10, 100]. If the user enters a number outside of this range, the program will continue asking.

2. In each turn, the player will be allowed to take from 1 to 3 sticks (without exceeding the number of available sticks on the table). If the user enters a number outside of this range, the program will continue asking.

3. When the game ends, the program should say who the winner is.

**Example:**

```
Welcome to the game of sticks!

Enter an initial number of sticks [10,100]: 150
Enter an initial number of sticks [10,100]: 10

There are 10 sticks on the table.
Player 1 - Enter the number of sticks to take (1-3): 4
Player 1 - Enter the number of sticks to take (1-3): 3

There are 7 sticks on the table.
Player 2 - Enter the number of sticks to take (1-3): 3

There are 4 sticks on the table.
Player 1 - Enter the number of sticks to take (1-3): 2

There are 2 sticks on the table.
Player 2 - Enter the number of sticks to take (1-2): 3
Player 2  - Enter the number of sticks to take (1-2): 1

There is 1 stick on the table.
Player 1 - Enter the number of sticks to take (1-1): 1
You lose, Player 1 :-(
```

### Exercise 2 - Write the Pseudocode

#### Instructions

1. Close the computer.

2. Express the algorithm in pseudocode.

3. Execute the pseudocode by hand to capture logical errors. Try it with various *extreme* cases and write the results by hand. [Here](https://docs.google.com/a/upr.edu/presentation/d/1m-lXCUTKhCZC8vTnmtAXwfxQjIKkSVhTUrcywiZsdbQ/edit?usp=sharing) you can see an example of how to hand check an algorithm.


### Exercise 3 - Implement the Code

#### Instructions 

1. Open the computer.

2. Implement the algorithm in a C++ program.

**Help:**

1) Use an `int` type variable to keep track of the player. At the end of each turn, change the player like this: 

   ```cpp
   player = player == 1 ? 2 : 1;

   ```
or number the players as 0 and 1 and use your mastery of the modulus operator:

   ```cpp
   player = (player + 1) % 2;

   ```

2) Think about the 3 commandments of repetition structures:

   a. What is the loop control variable and what condition will be used to iterate the loop?

   b. What value should the loop control variable be initialized to?

   c. How is the loop control variable modified during each iteration?

### Deliverables 

1. Use “Deliverables 1” in Moodle to turn in the pseudocode for the game of sticks.

2. Use “Deliverables 2” in Moodle to turn in the paper where you and your partner executed the pseudocode by hand. 

3. Use “Deliverables 3” in Moodle to turn in a `.cpp` file with the C++ program to play the game of sticks. The program should not have compiler errors, should be documented and function as intended. Remember to use good programming techniques, include the name of the programmers, and document your program.



---


---

##References

[1] http://nifty.stanford.edu/2014/laaksonen-vihavainen-game-of-sticks/handout.html

[2] http://www.gardengames.com/acatalog/509_Garden_Games_Giant_Pick-Up_Sticks.html

[3] http://www.toves.org/books/java/ch05-while/

[4] http://forum.codecall.net/topic/35425-pseudocode-tutorial-the-basics/page-2

---

---

---
